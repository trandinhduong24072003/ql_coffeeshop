-- Thiết lập ngữ cảnh cơ sở dữ liệu
USE qlcafe;
GO

-- Bảng 'ban'
CREATE TABLE ban (
  MaBan INT PRIMARY KEY IDENTITY(1,1),
  TenBan NVARCHAR(55) NOT NULL,
  TrangThai NVARCHAR(50) NOT NULL
);

INSERT INTO ban (TenBan, TrangThai) VALUES
( 'Bàn 1', 'Đã đặt trước'),
( 'Bàn 2', 'Đang phục vụ'),
( 'Bàn 3', 'Trống'),
( 'Bàn 4', 'Đã đặt trước'),
( 'Bàn 5', 'Trống'),
( 'Bàn 6', 'Trống'),
( 'Bàn 7', 'Trống'),
( 'Bàn 8', 'Đang phục vụ'),
( 'Bàn 9', 'Trống'),
( 'Bàn 10', 'Đang phục vụ'),
( 'Bàn 11', 'Đã đặt trước'),
( 'Bàn 12', 'Trống'),
( 'Bàn 13', 'Đang phục vụ'),
( 'Bàn 14', 'Trống'),
( 'Bàn 15', 'Trống'),
( 'Bàn 16', 'Đang phục vụ'),
( 'Bàn 17', 'Trống'),
( 'Bàn 18', 'Trống'),
( 'Bàn 19', 'Đã đặt trước'),
( 'Bàn 20', 'Trống');

-- Bảng 'hoadon'
CREATE TABLE hoadon (
  MaHoaDon INT PRIMARY KEY IDENTITY(159,1),
  GiamGia INT,
  MaBan INT NOT NULL,
  GioDen DATETIME NOT NULL,
  TongTien INT,
  TrangThai INT NOT NULL,
  CONSTRAINT FK_HoaDon_Ban FOREIGN KEY (MaBan) REFERENCES ban(MaBan)
);

INSERT INTO hoadon (GiamGia, MaBan, GioDen, TongTien, TrangThai) VALUES
(NULL,   5, '2016-02-17 19:44:48', 250000, 1),
(NULL,  17, '2016-01-17 19:45:04', 600000, 1),
(NULL,   13, '2016-01-17 19:45:13', 240000, 1),
(NULL,   6, '2016-03-17 19:45:20', 50000, 1),
(NULL,   9, '2016-03-17 19:45:33', 2500000, 1),
(NULL,   1, '2016-03-17 19:50:24', 25000, 1),
(NULL,   9, '2016-03-17 19:50:28', 25000, 1),
(NULL,   13, '2016-03-17 19:50:33', 25000, 1),
(NULL,   7, '2016-02-17 19:50:42', 25000, 1),
(NULL,   1, '2016-03-17 20:14:16', 6000000, 1),
(NULL,   9, '2016-03-17 20:14:37', 102000, 1),
(NULL,   8, '2016-03-17 20:14:47', 1000000, 1),
(NULL,   16, '2016-03-17 20:14:59', 40000, 1),
(NULL,   1, '2016-03-17 20:15:37', 35000, 1),
(NULL,  14, '2016-03-17 23:47:38', 0, 1),
(NULL,  5, '2016-03-17 23:52:59', 89700, 1),
(NULL,   17, '2016-03-17 23:53:14', 30000, 1),
(NULL,  7, '2016-03-17 23:53:50', 14250, 1),
(NULL, 2, '2016-03-17 23:54:01', 35000, 1),
(NULL, 9, '2016-03-18 00:11:27', 66500, 1),
(NULL,  14, '2015-12-18 00:11:57', 225000, 1),
(NULL,   14, '2015-09-18 00:15:15', 30000, 1),
(NULL,   17, '2015-07-18 00:15:20', 30000, 1),
(NULL, 2, '2016-02-18 00:15:25', 156000, 1),
(NULL,   8, '2016-01-18 00:15:31', 25000, 1),
(NULL,   20, '2016-03-18 00:15:42', 50000, 1),
(NULL,   3, '2016-03-18 09:17:29', 20000, 1),
(NULL,  8, '2016-03-18 09:28:01', 13500, 1),
(NULL,   17, '2016-03-18 09:28:05', 20000,  0),
(NULL,  6, '2016-03-18 09:28:09', 70000, 1),
(NULL,  2, '2016-03-18 09:34:47', 36000, 1),
(NULL,  18, '2016-03-30 02:37:43', 120000, 1),
(NULL,  19, '2016-03-30 03:18:39', 108000, 1),
(NULL,   16, '2016-03-30 09:25:48', 20000, 1),
(NULL,  15, '2016-04-01 17:25:54', 48000, 1),
(NULL,  9, '2016-04-01 17:27:43', 140000, 1),
(NULL,  8, '2016-04-06 11:17:57', 13500,  0),
(NULL,   2, '2016-04-06 11:18:04', 13500,  0),
(NULL,   10, '2016-04-06 11:18:12',13500,   0),
(NULL,   14, '2016-04-06 11:18:37',13500,   0),
(NULL,   18, '2016-04-06 11:23:10', 200000, 1);

-- Bảng 'nhommon'
CREATE TABLE nhommon (
  MaLoai INT PRIMARY KEY IDENTITY(1,1),
  TenLoai NVARCHAR(55) NOT NULL,
  MauSac NVARCHAR(50) NOT NULL
);

INSERT INTO nhommon (TenLoai, MauSac) VALUES
('Cà phê', '#ac3939'),
('Nước đóng chai', '#66b3ff'),
('Nước-Lon', '#9900ff'),
('Lipton-Trà', '#ffcc00'),
('Sinh tố', '#40ff00'),
('Thứ khác', '#e68a00'),
('Đồ ăn nhanh', '#009966');

-- Bảng 'taikhoan'
CREATE TABLE taikhoan (
  id INT PRIMARY KEY IDENTITY(1,1),
  username NVARCHAR(30) NOT NULL,
  password NVARCHAR(30) NOT NULL,
  lv INT NOT NULL
);

INSERT INTO taikhoan (username, password, lv) VALUES
( 'thanggun99', 'a', 1),
( 'nhanvien', '1', 2),
( 'nhanvien2', '1', 2),
( 'admin', 'admin', 1);

-- Bảng 'thucdon'
CREATE TABLE thucdon (
  MaMon INT PRIMARY KEY IDENTITY(1,1),
  TenMon NVARCHAR(55) NOT NULL,
  MaLoai INT NOT NULL,
  DonGia INT NOT NULL,
  DVT NVARCHAR(55) NOT NULL,
  CONSTRAINT FK_ThucDon_NhomMon FOREIGN KEY (MaLoai) REFERENCES nhommon(MaLoai)
);

INSERT INTO thucdon (TenMon, MaLoai, DonGia, DVT) VALUES
('Nâu đá', 1, 25000, 'Ly'),
('Nâu nóng', 1, 25000, 'Ly'),
('Cafe Sữa', 1, 50000, 'Ly'),
( 'Lọc đá vắt chanh', 2, 40000, 'Chậu'),
( 'Nâu lắc', 1, 69000, 'Ly'),
( 'Trà Xanh ', 2, 25000, 'Chai'),
( 'Trà C2', 2, 20000, 'Chai'),
( 'Chanh muối', 2, 20000, 'Chai'),
( 'Coca Cola', 3, 25000, 'Lon'),
( 'RedBull', 3, 25000, 'Lon'),
( 'Pepsi', 3, 20000, 'Lon'),
( 'Trà Gừng', 4, 25000, 'Ly'),
( 'Trà Dilmah', 4, 25000, 'Ly'),
( 'Trà chanh', 4, 15000, 'Ly'),
( 'Trà My', 4, 200000, 'Bát'),
( 'Sinh tố Xoài', 5, 30000, 'Ly'),
( 'Sinh tố bơ', 5, 35000, 'Ly'),
( 'Sinh tố Dưa Hấu', 5, 30000, 'Ly'),
( 'Sinh tố Mãng Cầu', 5, 35000, 'Ly'),
( 'Sinh tố chanh leo', 5, 30000, 'Ly'),
( 'Sinh tố dưa chuột', 5, 35000, 'Ly'),
( 'kẹo cao su', 6, 1000, 'cái'),
( 'Hướng Dương', 6, 15000, 'Đĩa'),
( 'Khoai chiên', 6, 15000, 'Miếng'),
( 'Vina', 6, 30000, 'Bao'),
( '555', 6, 60000, 'Bao'),
( 'Thăng Long', 6, 20000, 'Bao'),
( 'Cao cao nóng', 1, 25000, 'Ly'),
( 'Ca cao nguội', 1, 25000, 'Ly'),
( 'Đen đá', 1, 25000, 'Ly'),
( 'Đen nóng ', 1, 25000, 'Ly'),
( 'Bia Ken', 3, 25000, 'Lon'),
( 'Bia Sài Gòn', 3, 20000, 'Lon'),
( 'Bia Hà Nội', 3, 20000, 'Lon'),
( 'Bia Kenn', 3, 25000, 'Lon'),
( 'Bia Ken', 3, 25000, 'Lon'),
( 'Mỳ tôm', 7, 15000, 'Bát'),
( 'Bánh mỳ pate', 7, 15000, 'Cái'),
( 'Mực nướng', 7, 55000, 'Con');



-- Bảng 'chitiethd'
CREATE TABLE chitiethd (
  MaChiTietHD INT PRIMARY KEY IDENTITY(1,1),
  MaHoaDon INT NOT NULL,
  MaMon INT NOT NULL,
  SoLuong INT NOT NULL,
  Gia INT NOT NULL,
  CONSTRAINT FK_ChiTietHD_HoaDon FOREIGN KEY (MaHoaDon) REFERENCES hoadon(MaHoaDon),
  CONSTRAINT FK_ChiTietHD_ThucDon FOREIGN KEY (MaMon) REFERENCES thucdon(MaMon)
);

INSERT INTO chitiethd (MaHoaDon, MaMon, SoLuong, Gia) VALUES
( 159, 9, 5, 50000),
( 160, 32, 10, 60000),
( 161, 39, 12, 20000),
( 162, 16, 2, 25000),
( 163, 21, 5, 500000),
( 164, 15, 1, 25000),
( 165, 7, 1, 25000),
( 166, 37, 1, 25000),
( 168, 7, 1, 25000),
( 170, 21, 12, 500000),
( 172, 10, 3, 40000),
( 173, 21, 2, 500000),
( 174, 33, 2, 20000),
( 176, 27, 1, 35000),
( 177, 14, 1, 20000),
( 179, 11, 2, 69000),
( 180, 29, 2, 15000),
( 181, 20, 1, 15000),
( 182, 39, 2, 25000),
( 183, 23, 2, 35000),
( 184, 17, 1, 20000),
( 184, 39, 1, 25000),
( 184, 34, 3, 25000),
( 184, 35, 2, 25000),
( 184, 30, 2, 15000),
( 184, 8, 2, 25000),
( 185, 26, 1, 30000),
( 186, 26, 1, 30000),
( 187, 17, 1, 20000),
( 188, 34, 1, 25000),
( 189, 35, 2, 25000),
( 187, 9, 1, 50000),
( 187, 36, 3, 25000),
( 187, 37, 2, 25000),
( 190, 17, 1, 20000),
( 191, 20, 1, 15000),
( 192, 11, 1, 69000),
( 193, 39, 1, 20000),
( 195, 15, 1, 15000),
( 195, 39, 1, 25000),
( 196, 32, 2, 60000),
( 197, 32, 2, 60000),
( 198, 32, 1, 20000),
( 192, 26, 1, 30000),
( 192, 27, 1, 35000),
( 192, 22, 1, 30000),
( 192, 39, 1, 55000),
( 192, 39, 1, 15000),
( 192, 39, 1, 15000),
( 192, 32, 1, 60000),
( 192, 33, 1, 20000),
( 192, 31, 1, 30000),
( 192, 30, 1, 15000),
( 192, 28, 1, 250000),
( 192, 29, 1, 15000),
( 192, 36, 1, 25000),
( 192, 9, 1, 50000),
( 193, 39, 1, 20000),
( 193, 39, 1, 25000),
( 193, 16, 1, 25000);

